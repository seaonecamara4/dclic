class Book {
  String? title;
  List<String>? authors;

  String getTitle() {
    return title!;
  }

  List<String> getAuthors() {
    return authors!;
  }

  void addAuthors(String? name) {
    authors = authors ?? <String>[];
    authors!.add( name!);
  }

  Map<String, dynamic> toMap() {
    return {"title": title, "authors": authors};
  }
}
