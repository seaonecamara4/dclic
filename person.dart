class Person {
  String? name;
  DateTime? birthday;

  String getName() {
    return name!;
  }

  void setName(String name) {
    this.name = name;
  }

  bool isBirthday() {
    return true;
  }

  Map<String, dynamic> toMap(Person person) {
    return {"name": name, "birthday": birthday};
  }
}
