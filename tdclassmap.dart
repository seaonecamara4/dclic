import 'model/person.dart';
import 'model/book.dart';

void main(List<String> arguments) {
  List<Person> personsList = <Person>[];

  Person person = Person();
  person.setName("Bailly");
  Person person2 = Person();
  person2.setName("christelle");

  personsList.add(person);
  personsList.add(person2);

  List<Map<String, dynamic>> personMap =
      personsList.map((e) => e.toMap(person)).toList();

  List<Book> booksList = <Book>[];
  Book book1 = Book();
  book1.addAuthors("pauline");
  booksList.add(book1);

  List<Map<String,dynamic>> bookMap =booksList.map((e) => e.toMap()).toList();

  print(personMap);
  print(bookMap);
}
